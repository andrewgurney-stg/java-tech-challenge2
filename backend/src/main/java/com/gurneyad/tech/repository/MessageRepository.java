package com.gurneyad.tech.repository;

import com.gurneyad.tech.data.Message;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MessageRepository extends PagingAndSortingRepository<Message, Long> {

}
