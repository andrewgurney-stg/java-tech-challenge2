package com.gurneyad.tech.service;

import com.gurneyad.tech.data.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MessageService {

    Page<Message> retrieveMessages(Pageable pageRequest);

    Message retrieveUser(Long id);

    Message saveMessage(Message message);
}
