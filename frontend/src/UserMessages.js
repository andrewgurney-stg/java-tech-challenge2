import React from "react";

class UserMessages extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedUser: '',
            // users: [],
            messages: [],
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let value = event.target.value;
        console.log(value);
        this.setState((state) => {
            this.updateMessages(value);
            return {selectedUser: value};
        });
    }

    updateMessages(selectedUser) {
        // make sure that we are not trying to load messages on null
        if (selectedUser !== '') {
            fetch("http://localhost:8080/users/" + selectedUser)
                .then(results => {
                    console.log(results);
                    return results.json()
                }).then(data => {
                let messageData = data.messages.map((message) => {
                    return (
                        <div>{message.message}</div>
                    )
                });
                this.setState({messages: messageData});
            })
        }
    }

    componentDidMount() {
        this.props.updateUsersList();
    }

    render() {
        return (
            <div>
                <label><b>Select the user to see their messages: </b></label>
                <select onChange={this.handleChange}>
                    <option disabled selected value> -- select an option -- </option>
                    {this.props.users}
                </select><br/>
                <label>Messages: </label><br/>
                <div>{this.state.messages}</div>
            </div>
        )
    }
}

export default UserMessages;