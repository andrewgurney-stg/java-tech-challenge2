import React, { Component } from 'react';
import './App.css';

import UsernameForm from "./UsernameForm";
import MessageSender from "./MessageSender";
import UserMessages from "./UserMessages";
import AllMessages from "./AllMessages";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedUser: '',
            users: [],
            messages: [],
        };
        this.updateUsersList = this.updateUsersList.bind(this);
    }

    render() {
        return (
            <div>
                <UsernameForm updateUsersList={this.updateUsersList}/><br/>
                <MessageSender users={this.state.users}/><br/>
                <UserMessages updateUsersList={this.updateUsersList} users={this.state.users}/><br/>
                <AllMessages/>
            </div>
        )
    }

    updateUsersList = () => {
        console.log('Updating list of users');
        fetch('http://localhost:8080/users')
            .then(results => {
                let json = results.json();
                console.log(json);
                return json;
            }).then(data => {
            let userData = data.content.map((record) => {
                return (
                    <option value={record.userId}>{record.username}</option>
                )
            });
            this.setState({users: userData});
        });
    }
}

export default App;
