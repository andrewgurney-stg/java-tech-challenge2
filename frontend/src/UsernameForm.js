import React from "react";

class UsernameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        // alert('A name was submitted: ' + this.state.value);
        console.log(JSON.stringify({
            username: this.state.value,
            active: true,
        }));
        fetch('http://localhost:8080/users', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.value,
                active: true,
            })
        }).then((result) => {
            console.log(result);
            this.props.updateUsersList();
        });
        event.preventDefault();
    }

    render() {
        return (
            <form className="UserForm" onSubmit={this.handleSubmit}>
                <label>
                    <b>
                        Enter a username that you would like to add to the system
                    </b>
                </label><br/>
                <label>
                    Username:
                    <input type="text" value={this.state.value} onChange={this.handleChange}/>
                </label>
                <input type="submit" value="Submit"/>
            </form>
        );
    }
}

export default UsernameForm;