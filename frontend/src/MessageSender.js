import React from "react";

class MessageSender extends React.Component {
    constructor(props) {
        super(props);
        this.state = {message: '',
            toUser: '',
            fromUser: '',
        };
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFromChange(event) {
        if (event !== null && event.target !== null) {
            console.log("About to set From User: " + event.target.value);
            this.setState({fromUser: event.target.value});
        }
    }

    handleToChange(event) {
        if (event !== null && event.target !== null) {
            console.log("About to set To User: " + event.target.value);
            this.setState({toUser: event.target.value});
        }
    }

    handleMessageChange(event) {
        if (event !== null && event.target !== null) {
            this.setState({message: event.target.value});
        }
    }

    handleSubmit(event) {
        console.log('About to send a message');
        console.log("Here is the body of the call: " + JSON.stringify({
            message: this.state.message,
            toUser: this.state.toUser,
            fromUser: this.state.fromUser,
        }));
        if (this.state.toUser !== '' && this.state.fromUser !== '' && this.state.message !== '') {
            fetch('http://localhost:8080/messages', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    message: this.state.message,
                    toUser: this.state.toUser,
                    fromUser: this.state.fromUser,
                })
            }).then((result) => {
                console.log(result);
            });
        }
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    <b>
                        Send a Message:
                    </b>
                </label><br/>
                <label>
                    From Username:
                    <select onChange={this.handleFromChange}>
                        <option disabled selected value> -- select an option -- </option>
                        {this.props.users}
                    </select>
                </label><br/>
                <label>
                    To Username:
                    <select onChange={this.handleToChange}>
                        <option disabled selected value> -- select an option -- </option>
                        {this.props.users}
                    </select>
                </label><br/>
                <label>
                    Message:
                    <input type="text" value={this.state.message} onChange={this.handleMessageChange}/>
                </label><br/>
                <input type="submit" value="Submit"/>
            </form>
        );
    }
}

export default MessageSender;