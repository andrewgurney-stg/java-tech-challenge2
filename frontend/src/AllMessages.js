import React, {Component} from 'react';

class AllMessages extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            offset: 0,
            pageSize: 10,
            totalPages: 1,
            paging: '',
        }
    }

    callPageChange(pageIndex) {
        console.log("Page Change" + pageIndex);
        this.updatePage(pageIndex);
    }

    updatePage(pageIndex) {
        fetch("http://localhost:8080/messages?size=" + this.state.pageSize + "&page=" + pageIndex)
            .then(results => {
                console.log(results);
                return results.json()
            }).then(data => {
            let totalPages = data.totalPages;
            let messageData = data.content.map((message) => {
                return (
                    <div>{message.message}</div>
                )
            });
            let paging = [];
            for (let i = 0; i < data.totalPages; i++) {
                paging.push(<button onClick={() => this.callPageChange(i)}>{i+1}</button>);
            }
            let offset = data.number;
            this.setState({
                data: messageData,
                totalPages: totalPages,
                paging: paging,
                offset: offset,
            });
        })

    }


    callEndpoint(pageIndex) {
        console.log("Call Endpoint");
        fetch("http://localhost:8080/messages?size=" + this.state.pageSize + "&page=" + pageIndex)
            .then(results => {
                console.log(results);
                return results.json()
            }).then(data => {
            let totalPages = data.totalPages;
            let messageData = data.content.map((message) => {
                return (
                    <div>{message.message}</div>
                )
            });
            let paging = [];
            for (let i = 0; i < data.totalPages; i++) {
                paging.push(<button onClick={() => this.callPageChange(i)}>{i+1}</button>);
                // paging += <button onClick={this.callPageChange(i)}>({i+1})</button>;
            }
            let offset = data.number;
            this.setState({
                data: messageData,
                totalPages: totalPages,
                paging: paging,
                offset: offset,
            });
        })
    }

    componentDidMount() {
        this.callEndpoint(this.state.offset);
    }

    render() {
        return <div>
            <label>All Messages:</label><br/>
            <div>{this.state.data}</div><br/>
            <div>{this.state.paging}</div>
        </div>
    }
}

export default AllMessages;